package com.asymu;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Stock {
    public static void main(String[] args) {
        method();
    }

    static void method() {
        int[] stockPrices = new int[] {350, 450, 275, 435, 366, 786, 235};

        ArrayList<Integer> arrInt = IntStream.of(stockPrices).boxed().collect(Collectors.toCollection(ArrayList::new));
        List<Integer> listInt = IntStream.of(stockPrices).boxed().collect(Collectors.toList());
        ArrayList<Integer> arrListInt = Arrays.stream(stockPrices).boxed().collect(Collectors.toCollection(ArrayList::new));
    }
}
