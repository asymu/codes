package com.asymu;

import java.util.ArrayList;
import java.util.List;

public class Runner {
    public static void main(String[] args) {
        List<Person> pList = new ArrayList<>();
        pList.add(new Person("Syed1",21));
        pList.add(new Person("Syed2",20));
        pList.add(new Person("Syed3",21));
        pList.add(new Person("Syed4",11));

        pList.removeIf(e->e.getAge() < 20);
        System.out.println(pList);
        }
}
