package com.asymu;

import java.util.Stack;

public class BalancedBraces {
    public static void main(String[] args) {
        System.out.println(ifBalanced() ? "Balanced" : "Non Balanced");
    }

    private static boolean ifBalanced() {
        String exp ="{[()]}";
        Stack<Character> holder = new Stack();

        for (int i = 0; i < exp.length(); i++) {
            char pos = exp.charAt(i);
            if (pos == '{' || pos == '[' || pos == '(') {
                holder.push(pos);
                continue;
            }

            if (holder.isEmpty()){
                return false;
            }

            switch (pos) {
                case ')' : {
                    char popEle = (char) holder.pop();
                    if ( popEle == '{' || popEle == '[') {
                        return false;
                    }
                    break;
                }

                case '}' : {
                    char popEle = (char) holder.pop();
                    if ( popEle == '(' || popEle == '[') {
                        return false;
                    }
                    break;
                }

                case ']' : {
                    char popEle = (char) holder.pop();
                    if ( popEle == '{' || popEle == '(') {
                        return false;
                    }
                    break;
                }
            }

        }

        return holder.isEmpty();
    }
}
